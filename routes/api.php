<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware'=>'sessions'],function(){

    Route::post('auth','Auth\LoginController@auth');

    Route::get('products','ProductController@index');

    Route::get('cart','ShoppingCartController@index')->middleware('sessions');

    Route::put('cart/add/{product_id}','ShoppingCartController@addProduct');

    Route::delete('cart/{product_id}','ShoppingCartController@removeProduct');

    Route::post('cart/clear','ShoppingCartController@clearCart');
});
