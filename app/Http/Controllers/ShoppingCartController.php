<?php

namespace App\Http\Controllers;
use App\Product;
use Illuminate\Http\Request;
use Session;
class ShoppingCartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $cart = collect($request->session()->get('cart'));
        return $cart->push([
            'total'=> $cart->sum(function($value){
                return $value['qty'] * $value['price'];
            }),
            'total_formated' => "R$ ".number_format($cart->sum(function($value){
                return $value['qty'] * $value['price'];
            })/100,2,',','.')
        ])->values();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function addProduct(Request $request)
    {
        $product = Product::find($request->product_id);
        if(!$product){
            return response('product does not exist',404);
        }
        $cart = collect($request->session()->get('cart'));
        if(!$cart->contains('product_id',$product->id)){
            // return 'ok';
            $request->session()->push('cart',[
                'product_id' =>$product->id,
                'product'=>$product->name,
                'price'=>$product->price,
                'qty' => 1
            ]);
        }else{
            $cart = $cart->map(function($val,$index) use($product){
               if($product->id == $val['product_id']){
                   $val['qty']++;
               }
                return $val;
            });

            $request->session()->put('cart',$cart);
        }

        $cart = collect($request->session()->get('cart'));
        return $cart->push([
            'total'=> $cart->sum(function($value){
                return $value['qty'] * $value['price'];
            }),
            'total_formated' => "R$ ".number_format($cart->sum(function($value){
                return $value['qty'] * $value['price'];
            })/100,2,',','.')
        ])->values();
    }

    /**
     * Store a newly created resource in storage.
     *destroy
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function removeProduct(Request $request)
    {
        $cart = collect($request->session()->get("cart"));
        //verificando se o produto existe no carrinho
        if($cart->contains('product_id',$request->product_id)){
            //verificando se a quantidade do produto que está no carrinho é maior que 1
            if($cart->firstWhere('product_id',$request->product_id)['qty'] > 1){
                $cart = $cart->map(function($value,$key) use($request){
                    if($value['product_id'] == $request->product_id){
                        $value['qty']--;
                    }

                    return $value;
                });
                $request->session()->put('cart',$cart);
            }else{
                $cart = $cart->filter(function($value,$key) use($request){
                    return $value['product_id'] != $request->product_id;
                });
                $request->session()->put('cart',$cart);
            }
            $cart = collect($request->session()->get('cart'));
            return $cart->push([
                'total'=> $cart->sum(function($value){
                    return $value['qty'] * $value['price'];
                }),
                'total_formated' => "R$ ".number_format($cart->sum(function($value){
                    return $value['qty'] * $value['price'];
                })/100,2,',','.')
            ])->values();
        }

        return response('product does not exist in cart',404);
    }

    public function clearCart(Request $request){
        $request->session()->forget('cart');
        return $request->session()->get('cart',[]);
    }

}
