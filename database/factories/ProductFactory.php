<?php

use Faker\Generator as Faker;
use Carbon\Carbon;
$factory->define(App\Product::class, function (Faker $faker) {
    $faker = \Faker\Factory::create();
    $faker->addProvider(new \FakerRestaurant\Provider\pt_BR\Restaurant($faker));
    return [
        'name'=>$faker->fruit(),
        'price'=>$faker->randomNumber(3),
        'created_at'=> Carbon::now()
    ];
});
