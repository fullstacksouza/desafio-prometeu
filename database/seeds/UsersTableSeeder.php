<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')
        ->insert([
            'name' => 'Matheus Souza',
            'email'=> 'matheus.souzadv@gmail.com',
            'email_verified_at' => Carbon::now(),
            'password'=>bcrypt('password'),
            'created_at'=> Carbon::now()
        ]);
    }
}
