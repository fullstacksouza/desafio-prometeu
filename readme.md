# desafio-prometeu

    O desafio proposto foi realizado utilizando o framework Laravel, para rodar o desafio certifique-se que possui os requisitos para utilização do framework

        * PHP >= 7.1.3
        * OpenSSL PHP Extension
        * PDO PHP Extension
        * Mbstring PHP Extension
        * Tokenizer PHP Extension
        * XML PHP Extension
        * Ctype PHP Extension
        * JSON PHP Extension
        * BCMath PHP Extension

## Instalaçao

```
cp .env.example .env
```

```
composer install
```

## Gerando chave da aplicação

```
php artisan key:generate
```

## Configurando banco de dados

arquivo .env

```
DB_CONNECTION=mysql
DB_HOST=127.0.0.1
DB_PORT=3306
DB_DATABASE=nomedobanco
DB_USERNAME=usuario
DB_PASSWORD=senha
```

## Criando as tabelas

```
php artisan migrate
```

## Rodando seeds

este comando ira inserir dados na tabela de produtos, e criar o usuario para
realizar o teste de autenticação
email : matheus.souzadv@gmail.com
password : password

```
php artisan db:seed
```

## Iniciando o server

```
php artisan serve
```
